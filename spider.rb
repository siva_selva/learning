		#Getting the product detais from a page
		# input-->url
		# output-->product details with image links
require 'metainspector'
require 'nokogiri'
require 'open-uri'

class Spider_man

     def initialize(name)
	@name=name
     end


     def boot(f_path_r,f_path_w)		# f_path_r-->file path for reading and f_path_w-->file path for writing
			File.open(f_path_w,'a+') do |f1|	#root tag of the products
		         f1.puts "<products>"
			end
				File.open(f_path_r,'r') do |f2|	#here Getting url from the file and call the get_content method
					f2.each {|line|
					get_content(line,f_path_w)
					}
				end
			File.open(f_path_w,'a+') do |f1|	#roor ending tag
		         f1.puts ""
		         f1.puts "</products>"
			end
     end
     
     
     def get_content(url,f_path_w)	# here this method Getting content from the given url and store to database
	     i=0
	     j=1
		page = Nokogiri::HTML(open(url))	
		File.open(f_path_w,'a+') do |f1|
			f1.puts ""
			f1.puts "<product>"		#product count
			f1.puts "<name>#{page.css('.style2').text}</name>" # here getting the name of the product if class = 'style2'
			#l1=page.search('.style2').map{|img| img.text}
			data = page.search('.specs').map{ |li|
				li.search('div').map { |div| div.text }	#here mapping(array of array )the product details to data if class='specs'
			}
			while i != data[0].length-1
				f1.puts "<#{data[0][i].split(" ").join("_").downcase}>#{data[0][j]}</#{data[0][i].split(" ").join("_").downcase}>" #here printing the map value to the file in format of xml
				j+=2
				i+=2
			end
			price=page.search('.price').map{|img| img.text.strip}
			price= price[0].slice!(8..-1).gsub(/[,.]/,'')
			f1.puts "<price>#{price}</price>"
			f1.puts "<images>"
			links = page.search('.bxslider').map{|li| li.search('img').map{|img| img['src']}}	#here getting link of images in  a page
			for i in 0..links[0].length-1
				f1.puts "<image>http://www.kasikiran.com/#{links[0][i]}</image>"
			end
			f1.puts "</images>"
			
			f1.puts "</product>"
		end	# end of the file
     end	# end of the method
     
     
 
end	# end of the class

spider=Spider_man.new('Kasikiran')
spider.boot('E:\Krizda\ruby_pgm\test.txt','E:\Krizda\ruby_pgm\product_data.xml')	#here to pass the file path for reading and writing
