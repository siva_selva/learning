def sum_even_fib(n)
    sumfib = 0
    a = 0
    b = 1
    while b < n do
        a = b
	b = a + b
        if b % 2 == 0
            sumfib = sumfib + b
        end
	# puts a,b,sumfib
    end
    return sumfib
end
 
puts sum_even_fib(4000000)
